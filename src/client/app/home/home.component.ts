import { Component, OnInit } from '@angular/core';
import { NameListService } from '../shared/index';
import { VideoRetrieveService } from '../shared/index';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css'],
})

export class HomeComponent implements OnInit {

  newUrl: string = "";
  errorMessage: string;
  urlList: any[] = [];
  videoList: any[] = [];
  videoToBuildList: any[] = [];
  selectedVideo:any = null;
  builtVideo:any=null;

  /**
   * Creates an instance of the HomeComponent with the injected
   * NameListService.
   *
   * @param {NameListService} nameListService - The injected NameListService.
   */
  constructor(public videoRetrieveService: VideoRetrieveService) {}

  /**
   * Get the urlList OnInit
   */
  ngOnInit() {
    //this.getVideos();
  }

  onUrlChange(e:any) {
    this.urlList = e;
    this.getVideos();
  }

  /**
   * Handle the nameListService observable
   */
  getVideos() {
    console.log("en home: ", this.urlList)
    this.videoRetrieveService.get(this.urlList)
      .subscribe(
        videoList => {
          this.videoList = videoList
          console.log("we get: ", videoList)  
          this.getFormattedVideos();        
          },
        error =>  this.errorMessage = <any>error
      );
  }

  getFormattedVideos() {
    this.activateLoading();
    let videoIdArr = this.videoList.map(v => {
      return {id:v.id};
    })
    console.log("my array de id es: ", videoIdArr);
    this.videoRetrieveService.getFormattedVideos(videoIdArr)
      .subscribe(
        videoList => {
          //this.videoList = videoList
          for(let i=0; i<videoList.length; i++) {
            this.videoList[i].source = videoList[i].url
          }
          this.deactivateLoading();
          console.log("we get: asdfasdfas", this.videoList)
          },
        error =>  this.errorMessage = <any>error
      );
  }

  onSelectVideo(video:any) {
    this.selectedVideo = video;
    var overlay = document.getElementById('dark-overlay');
    overlay.style.display = 'block';
    let self=this;
    overlay.onclick = function(e:any) {
      e.target.style.display="none";
      self.selectedVideo = null;
    }
  }

  onVideoSubmit(e:any) {
    this.videoToBuildList.push(e)
    var overlay = document.getElementById('dark-overlay');
    overlay.style.display = 'none';
    overlay.onclick = null;
    this.selectedVideo = null;
    console.log("videoToBuildList is: ", this.videoToBuildList);
  }

  onBuild() {
    this.activateLoading();
    let videoArr = this.videoToBuildList.map(v => {
      return {
        id: v.id,
        startTime: v.startTime,
        endTime: v.endTime,
      }
    })
    this.videoRetrieveService.getMergedVideo(videoArr)
      .subscribe(
        videoResult => {
          this.builtVideo = videoResult;
          this.builtVideo.image = this.videoToBuildList[0].image
          this.deactivateLoading();
          console.log("YEAAAH ", this.builtVideo)
          },
        error =>  this.errorMessage = <any>error
      );
  }

  activateLoading() {
    $("#spinner").css("display", "block")
    $("#dark-overlay").css("display", "block")
    $("#dark-overlay").on('click', () => {
      this.deactivateLoading();  
    })
  }

  deactivateLoading() {
    $("#spinner").css("display", "none")
    $("#dark-overlay").css("display", "none")
  }

  /**
   * Pushes a new name onto the urlList array
   * @return {boolean} false to prevent default form submit behavior to refresh the page.
   */

}
