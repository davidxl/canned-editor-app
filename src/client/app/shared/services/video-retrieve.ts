import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/observable/throw';
// import 'rxjs/add/operator/do';  // for debugging

/**
 * This class provides the NameList service with methods to read names and add names.
 */
@Injectable()
export class VideoRetrieveService {
myHeader:Headers = new Headers();
  /**
   * Creates a new NameListService with the injected Http.
   * @param {Http} http - The injected Http.
   * @constructor
   */
  constructor(private http: Http) {
    this.myHeader.append('Content-Type', 'application/json');
  }

  /**
   * Returns an Observable for the HTTP GET request for the JSON resource.
   * @return {string[]} The Observable for the HTTP request.
   */
  get(urlArr:string[]): Observable<any> {
    //let url = "http://canned-editor.mybluemix.net/articles/analyze";
    let url = "http://localhost:3000/articles/analyze";
    let params = JSON.stringify({articles:urlArr});
    return this.http.post(url, params, {headers: this.myHeader})
                    .map((res: Response) => res.json().videos)
    //              .do(data => console.log('server data:', data))  // debug
                    .catch(this.handleError);

    // return this.http.get('../assets/data.json')
    //                 .map((res: Response) => res.json().videos)
    // //              .do(data => console.log('server data:', data))  // debug
    //                 .catch(this.handleError);
  }

  getFormattedVideos(videoIdArr:any): Observable<any> {
    //let url = "http://canned-editor.mybluemix.net/articles/analyze";
    let url = "http://localhost:3000/video/urls";
    let params = JSON.stringify({videos:videoIdArr});
    
    return this.http.post(url, params, {headers: this.myHeader})
                    .map((res: Response) => res.json())
    //              .do(data => console.log('server data:', data))  // debug
                    .catch(this.handleError);

  }

  getMergedVideo(videoIdArr:any): Observable<any> {
    //let url = "http://canned-editor.mybluemix.net/articles/analyze";
    let url = "http://localhost:3000/video/build";
    let params = JSON.stringify({videos: videoIdArr});
    
    return this.http.post(url, params, {headers: this.myHeader})
                    .map((res: Response) => res.json())
    //              .do(data => console.log('server data:', data))  // debug
                    .catch(this.handleError);

  }

  /**
    * Handle HTTP error
    */
  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}