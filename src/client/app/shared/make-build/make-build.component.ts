import { Component, Output, EventEmitter, Input } from '@angular/core';

/**
 * This class represents the navigation bar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-make-build',
  templateUrl: 'make-build.component.html',
  styleUrls: ['make-build.component.css'],
})

export class MakeBuildComponent {
  @Output() build: EventEmitter<any> = new EventEmitter();
  @Input() video: any;

  mergeVideos() {
    this.build.next();
  }

  togglePlay(el:any) {
    el.paused?el.play():el.pause();
  }
}
