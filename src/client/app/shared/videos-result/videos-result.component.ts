import { Component, Input, Output, EventEmitter} from '@angular/core';

/**
 * This class represents the navigation bar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-videos-result',
  templateUrl: 'videos-result.component.html',
  styleUrls: ['videos-result.component.css'],
})

export class VideosResultComponent {
  @Input() videoList: any;
  @Output() selection: EventEmitter<string[]> = new EventEmitter();

  selectVideo(video:any) {
    this.selection.next(video);
  }
  
}
