import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * This class represents the navigation bar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-video-preview',
  templateUrl: 'video-preview.component.html',
  styleUrls: ['video-preview.component.css'],
})

export class VideoPreviewComponent {
  @Input() video: any;
  @Output() submition: EventEmitter<any> = new EventEmitter();
  startTime:string="";
  endTime:string="";
  //mask: any = [/([01][0-9]|[2][0-3])/,":",/([01345][0-9])/,":",/([01345][0-9])/]

  togglePlay(el:any) {
    el.paused?el.play():el.pause();
  }

  sendToBuild() {
    if(this.startTime && this.endTime) {
      let videoObj = this.video;
      videoObj.startTime = this.startTime;
      videoObj.endTime = this.endTime;
      this.submition.next(videoObj);
    }
     else {
       alert("only numeric values are allowed for start time / end time");
     }
 }

}
