/**
 * This barrel file provides the exports for the shared resources (services, components).
 */
export * from './name-list/index';
export * from './navbar/index';
export * from './toolbar/index';
export * from './config/env.config';
export * from './services/video-retrieve';
export * from './make-build/make-build.component';
export * from './url-list/url-list.component';
export * from './videos-to-build/videos-to-build.component';
export * from './url-list/url-list.component';
export * from './video-preview/video-preview.component';
export * from './videos-result/videos-result.component';
