import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ToolbarComponent } from './toolbar/index';
import { NavbarComponent } from './navbar/index';
import { NameListService } from './name-list/index';
import { VideoRetrieveService } from './index';
import { VideosToBuildComponent } from './index';
import { VideosResultComponent } from './index';
import { VideoPreviewComponent } from './index';
import { UrlListComponent } from './index';
import { MakeBuildComponent } from './index';


/**
 * Do not specify providers for modules that might be imported by a lazy loaded module.
 */

@NgModule({
  imports: [CommonModule, RouterModule, FormsModule],
  declarations: [ToolbarComponent, NavbarComponent,
    VideosToBuildComponent, VideosResultComponent,
    VideoPreviewComponent, UrlListComponent, MakeBuildComponent],
  exports: [ToolbarComponent, NavbarComponent,
    CommonModule, FormsModule, RouterModule,
    VideosToBuildComponent, VideosResultComponent,
    VideoPreviewComponent, UrlListComponent, MakeBuildComponent]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [NameListService, VideoRetrieveService]
    };
  }
}
