import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * This class represents the navigation bar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-url-list',
  templateUrl: 'url-list.component.html',
  styleUrls: ['url-list.component.css'],
})


export class UrlListComponent {
  urlList: string[] = [];
  newUrl: string = "";
  @Output() submition: EventEmitter<string[]> = new EventEmitter();

  ngOnInit() {

  }

  addUrl(): boolean {
    // TODO: implement nameListService.post
    this.urlList.push(this.newUrl);
    this.newUrl = '';
    this.submition.next(this.urlList);
    return false;
  }

  removeUrl(url:string): boolean {
    // TODO: implement nameListService.post
    let index = this.urlList.indexOf(url);
    this.newUrl = '';
    this.urlList.splice(index, 1);
    this.submition.next(this.urlList);
    return false;
  }
}
