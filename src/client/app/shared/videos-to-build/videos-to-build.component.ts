import { Component, Input } from '@angular/core';

/**
 * This class represents the navigation bar component.
 */
@Component({
  moduleId: module.id,
  selector: 'sd-videos-to-build',
  templateUrl: 'videos-to-build.component.html',
  styleUrls: ['videos-to-build.component.css'],
})

export class VideosToBuildComponent {

  @Input() videosToBuildList: any;
  //ipsumLoremVideo = {image:'http://almostsecretblog.com/wp-content/uploads/2014/10/image8_1.png'};
    ipsumLoremVideo = {image:'../assets/video_icon.svg'};
  getVideoList() {
    let videoListCopy = this.videosToBuildList.map((e:any) => {return e});
    videoListCopy = videoListCopy.splice(0,5);
    let length = videoListCopy.length;
    for(let i = 0; i < 5-length; i++){
      videoListCopy.push(this.ipsumLoremVideo);
    }
    $('.video-image').height($('.video-image').width());
    return videoListCopy;
  }
  //http://www.framesdirect.com/images/global/video-placeholder-img.jpg
}
